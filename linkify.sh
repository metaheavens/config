current_folder=$(pwd)
file_to_move=$1

if [[ -z $file_to_move ]] 
then
    echo "Missing parameter"
    echo "Usage: linkify.sh /User/lwisniewski/.zshrc"
    exit 1
fi


if [[ $file_to_move == "$HOME"* ]] 
then
    echo 'File inside $HOME, it will be copied at the root of this repository'
    all_home_folder=$current_folder/all_home/
    file_to_move_without_home=${file_to_move//$HOME/}
    dir_to_create=$(dirname $file_to_move_without_home)
    mkdir -p $all_home_folder$dir_to_create
    mv $file_to_move $all_home_folder$file_to_move_without_home
    ln -s $all_home_folder$file_to_move_without_home $file_to_move
else
    echo 'File outside $HOME, it will be copied in the matching OS folder of this repository'
    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
        dir_to_create=$(dirname $file_to_move)
        mkdir -p linux/$dir_to_create
        mv $file_to_move $current_folder/linux$file_to_move
        ln -s $current_folder/linux$file_to_move $file_to_move
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        dir_to_create=$(dirname $file_to_move)
        mkdir -p mac/$dir_to_create
        mv $file_to_move $current_folder/mac$file_to_move
        ln -s $current_folder/mac$file_to_move $file_to_move
    fi
fi

