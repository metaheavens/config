all_home_files=$(find all_home -type f)
current_dir=$(pwd)

for file in $all_home_files
do
    file_no_prefix=${file#all_home/}
    home_file=$HOME/$file_no_prefix
    echo $file_no_prefix
    if [[ -L $home_file ]]
    then
        rm $home_file
    fi
    if [[ -f $home_file ]]
    then
        echo "$home_file exists, renamed to $home_file.old"
        mv $home_file $home_file.old
    fi

    dir_to_create=$(dirname $home_file)
    mkdir -p $dir_to_create
    ln -s $current_dir/$file $home_file
done
