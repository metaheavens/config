# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

#Use by kubectl completion
autoload -Uz compinit
compinit

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
# Path to your oh-my-zsh installation.
if [ "$(uname)" = "Darwin" ]; then
    export ZSH="$HOME/.oh-my-zsh"
    export PATH=/opt/homebrew/bin:$PATH
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    export ZSH="/usr/share/oh-my-zsh"
fi
# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="powerlevel10k/powerlevel10k"
# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )
# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"
# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"
# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"
# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"
# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13
# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true
# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"
# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"
# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"
# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"
# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"
# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder
# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(docker docker-compose git per-directory-history zsh-autosuggestions zsh-nvm evalcache kubectl)
source <(kubectl completion zsh)
source $ZSH/oh-my-zsh.sh
# User configuration
# export MANPATH="/usr/local/man:$MANPATH"
# You may need to manually set your language environment
# export LANG=en_US.UTF-8
# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
# Compilation flags
# export ARCHFLAGS="-arch x86_64"
# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
bindkey -v

bindkey -a / fzf-history-widget

alias global=per-directory-history-toggle-history

# Defer initialization of nvm until nvm, node or a node-dependent command is
# run. Ensure this block is only run once if .bashrc gets sourced multiple times
# by checking whether __init_nvm is a function.

if [ "$(uname)" = "Darwin" ]; then
    [ -f /opt/homebrew/etc/profile.d/autojump.sh ] && . /opt/homebrew/etc/profile.d/autojump.sh
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    # Auto jump
    [[ -s /etc/profile.d/autojump.sh ]] && source /etc/profile.d/autojump.sh
fi

alias vim="nvim"
alias vi="nvim"
alias k9s="k9s --readonly"

if [ "$(uname)" = "Darwin" ]; then
    source ~/.zshrc_datadog
fi

export FZF_DEFAULT_COMMAND='rg --files --hidden'

alias pretty_logs="jq -R 'fromjson? | select(type == \"object\")'"

export BAT_THEME="Solarized (light)"

tfdoc() {
    #!/usr/bin/env bash
    #
    # Usage:
    #
    # Lookup resource:
    #   $ tfdoc aws_iam_role
    #
    # Lookup data source:
    #   $ tfdoc -d aws_iam_role
    #

    set -e

    tftype="r"

    usage() {
        local ec="$1"
        cat <<EOF
        Lookup resource:
        $ tfdoc aws_iam_role
        Lookup data source:
        $ tfdoc -d aws_iam_role
EOF
    }

    while getopts ':dh' opt; do
        case $opt in
            d) tftype='d';;
            h) usage 0;;
        esac
    done
    shift $((OPTIND-1))

    if [[ -z $1 ]]; then
        echo 'Please let me know the resource/data source!'
        usage 1
    fi

    if [[ $(uname) == 'Darwin' ]]; then
        open_cmd='open'
    else
        open_cmd='xdg-open'
    fi

    provider="${1%%_*}"
    resource="${1#*_}"

    $open_cmd https://www.terraform.io/docs/providers/${provider}/${tftype}/${resource}
}

[ -s ~/.luaver/luaver ] && . ~/.luaver/luaver

# Android SDK for React native
export ANDROID_SDK_ROOT=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/emulator
export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
