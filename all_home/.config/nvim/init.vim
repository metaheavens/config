set nocompatible              " required
filetype off                  " required

call plug#begin('~/.vim/plugged')

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)

Plug 'tweekmonster/braceless.vim' " Provides text objects for Python and other indented languages.
Plug 'arouene/vim-matchit' " Enhances the % motion to match more than just single characters.
Plug 'preservim/nerdtree'
Plug 'MunifTanjim/nui.nvim' " Library for creating user interfaces in neovim.
Plug 'tpope/vim-fugitive' " A Git wrapper for Vim, enabling Git commands and integration.
Plug 'tpope/vim-surround' " Provides mappings to easily delete, change, and add surroundings.
Plug 'tpope/vim-rhubarb' " Complements vim-fugitive with GitHub integration.
Plug 'altercation/vim-colors-solarized' " The popular Solarized color scheme for Vim.
Plug 'sickill/vim-monokai' " Monokai color scheme for Vim.
Plug 'ajmwagar/vim-deus' " Another color scheme inspired by Atom's One Dark.
Plug 'google/vim-jsonnet' " Provides Jsonnet language support in Vim.
Plug 'shumphrey/fugitive-gitlab.vim' " GitLab extension for vim-fugitive.
Plug 'vim-scripts/AutoClose' " Automatically closes brackets, quotes, etc.
Plug 'hzchirs/vim-material' " Material design theme for Vim.
Plug 'nvim-lua/plenary.nvim' " A Lua library required by many neovim plugins.
Plug 'scalameta/nvim-metals', { 'branch': 'main' } " Language server plugin for Scala.
Plug 'hrsh7th/nvim-cmp', { 'branch': 'main' } " Autocompletion plugin for neovim.
Plug 'hrsh7th/cmp-nvim-lsp', { 'branch': 'main' } " LSP source for nvim-cmp completion.
Plug 'hrsh7th/vim-vsnip' " Snippet engine for Vim.
Plug 'hrsh7th/vim-vsnip-integ' " Integration for vim-vsnip with other plugins.
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' } " FZF sorter for Telescope plugin.
Plug 'mfussenegger/nvim-dap' " Debug Adapter Protocol (DAP) implementation for neovim.
Plug 'rcarriga/nvim-dap-ui' " A UI for nvim-dap.
Plug 'ray-x/lsp_signature.nvim' " Show function signature with LSP.
Plug 'nvim-telescope/telescope.nvim' " Fuzzy finder and picker plugin for neovim.
Plug 'pwntester/octo.nvim' " Plugin for working with GitHub issues and PRs.
Plug 'kyazdani42/nvim-web-devicons' " Adds icons to other plugins.
Plug 'neovim/nvim-lspconfig' " Configurations for built-in LSP client.
Plug 'folke/trouble.nvim' " Pretty list for viewing diagnostics, etc.
Plug 'padde/jump.vim' " Enhances Jumplist navigation.
Plug 'sainnhe/everforest' " A green based colorscheme.
Plug 'akinsho/toggleterm.nvim', {'tag' : '*'} " Toggles neovim terminals easily.
Plug 'folke/which-key.nvim' " Displays available keybindings.
Plug 'nvim-lualine/lualine.nvim' " A status line plugin for neovim.
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'} " Better syntax highlighting.
Plug 'milisims/nvim-luaref' " Lua reference for neovim.
Plug 'folke/neodev.nvim' " Configurations and completion for neovim Lua development.
Plug 'tototutu/my-new-plugin' " Custom user plugin (name suggests it's user-defined).
Plug 'folke/twilight.nvim' " Dim inactive portions of the code.
Plug 'folke/zen-mode.nvim' " Distraction-free coding mode.
Plug 'lewis6991/gitsigns.nvim' " Git integration showing changed lines in the gutter.
Plug 'hashivim/vim-terraform' " Terraform support for vim.
Plug 'ldelossa/litee.nvim' " Sidebar and UI elements for LSP and DAP.
Plug 'ldelossa/litee-calltree.nvim' " Calltree UI component for 'litee.nvim'.
Plug 'ianding1/leetcode.vim' " Solve LeetCode problems in Vim.
Plug 'stevearc/aerial.nvim' " Code outline window, shows symbols in a file.
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'ray-x/go.nvim'
Plug 'theHamsta/nvim-dap-virtual-text'
Plug 'leoluz/nvim-dap-go'
Plug 'nvim-neotest/nvim-nio' " needed for dap ui
Plug 'zbirenbaum/copilot.lua'
Plug 'CopilotC-Nvim/CopilotChat.nvim', { 'branch': 'canary' }
Plug 'numToStr/Comment.nvim'
Plug 'habamax/vim-godot'
Plug 'nvim-neotest/nvim-nio'
call plug#end()

" neovide config
let g:neovide_input_macos_alt_is_meta = v:true

" All Plugins added before the following line
let g:semanticTermColors = [28,1,2,3,4,5,6,7,25,9,10,34,12,13,14,15,16,125,124,19]
let g:leetcode_browser = "firefox"
let g:leetcode_solution_filetype = "scala"

filetype plugin indent on    " required
autocmd FileType python BracelessEnable -indent

let NERDTreeShowHidden=1

set encoding=utf-8

let python_highlight_all=1
syntax enable

set t_Co=256
set termguicolors

set background=dark    " Setting dark mode
let g:material_style='palenight'
colorscheme vim-material
let g:everforest_background = 'soft'

let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
set nu

"set clipboard=unnamed " MacOS
set clipboard=unnamedplus
set number
set relativenumber
set tabstop=4
set shiftwidth=4
set expandtab

let g:syntastic_c_include_dirs = ['/usr/include']

" Personal config

nnoremap <silent> <space>z :ZenMode<CR>
nnoremap <silent> <space>s :%!sqlformat --reindent --keywords upper --identifiers lower -<CR>
nnoremap <silent> <space>r :Telescope live_grep<CR>
nnoremap <silent> <space>wf :Telescope grep_string<CR>
nnoremap <silent> <space>f :Files<CR>
nnoremap <silent> <space>b :Telescope buffers<CR>
nnoremap <silent> <space>p :Telescope resume<CR>
nnoremap <silent> <space><space> :noh<CR>
nnoremap <silent> <leader>o :NERDTree %<CR>
nnoremap <leader>t <Plug>ToggleAutoCloseMappings

"NEOVIDE CONF Allow copy paste in neovim
let g:neovide_input_use_logo = 1
map <D-v> "+p<CR>
map! <D-v> <C-R>+
tmap <D-v> <C-R>+
vmap <D-c> "+y<CR> 

" g:neovide_transparency should be 0 if you want to unify transparency of content and title bar.
let g:neovide_transparency = 0.8
"END OF NEOVIDE CONF

nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

nnoremap <space>gb :GBrowse!<CR>
nnoremap <space>gs :Telescope git_status<CR>
nnoremap <space>opr :Octo pr list<CR>
nnoremap <space>ors :Octo review start<CR>

vnoremap <space>gb :'<,'>GBrowse!<CR>

nnoremap <silent> ff    <cmd>lua vim.lsp.buf.format()<CR>
autocmd BufWritePre *.scala lua vim.lsp.buf.format()

tnoremap <C-Esc> <C-\><C-n>

set shell=zsh

lua require('init')
