local api = vim.api
local cmd = vim.cmd

-- Helper function for creating keymaps
function nnoremap(rhs, lhs, bufopts, desc)
  bufopts.desc = desc
  vim.keymap.set("n", rhs, lhs, bufopts)
end

local function map(mode, lhs, rhs, opts)
  local options = { noremap = true }
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  api.nvim_set_keymap(mode, lhs, rhs, options)
end

----------------------------------
-- OPTIONS -----------------------
----------------------------------
-- global
vim.opt_global.completeopt = { "menuone", "noinsert", "noselect" }
vim.opt.shortmess:remove("F")
vim.opt.shortmess:append("c")

vim.diagnostic.config({
  virtual_text = false
})

function _G.closest_action()
  local lineNum = vim.fn.getcurpos()[2]
  local t = 0
  local currentLens = {}
  local lenses = vim.lsp.codelens.get(0)
  vim.pretty_print(lineNum)
  
  for k, lens in ipairs(lenses) do 
      if (t == 0 or math.abs(lens.range.start.line - lineNum) < t) then
          currentLens = lens
          t = math.abs(lens.range.start.line - lineNum)
      end
  end

  local current = vim.fn.getcurpos()
  current[2] = currentLens.range.start.line + 1
  vim.fn.setpos('.', current)
  vim.lsp.codelens.run()
  return
end

-- Show line diagnostics automatically in hover window
vim.o.updatetime = 250
vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]

-- LSP mappings
map("n", "gd", "<cmd>lua require('telescope.builtin').lsp_definitions()<CR>")
map("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>")
map("n", "gi", "<cmd>lua require('telescope.builtin').lsp_implementations()<CR>")
map("n", "gr", "<cmd>lua require('telescope.builtin').lsp_references()<CR>")
map("n", "gws", "<cmd>lua require('telescope.builtin').lsp_dynamic_workspace_symbols()<CR>")
map("n", "<space>t", "<cmd>lua require('telescope.builtin').lsp_type_definitions()<CR>")
map("n", "<c-t>", "<cmd>ToggleTerm direction=float shading_factor=3<CR>")
map("n", "<space>w", [[<cmd>w<CR>]])
map("n", "<space>cl", [[<cmd>lua closest_action()<CR>]])
map("n", "<space>cp", [[<cmd>Copilot panel<CR>]])
map("n", "<space>cc", [[<cmd>CopilotChatToggle<CR>]])
map("n", "<space>sh", [[<cmd>lua vim.lsp.buf.signature_help()<CR>]])
map("n", "<leader>rn", "<cmd>lua vim.lsp.buf.rename()<CR>")
map("n", "<space>cf", "<cmd>lua vim.lsp.buf.formatting()<CR>")
map("n", "<space>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>")
map("n", "<space>ho", "<cmd>lua vim.lsp.buf.outgoing_calls()<CR>")
map("n", "<space>hi", "<cmd>lua vim.lsp.buf.incoming_calls()<CR>")
map("n", "<space>aa", [[<cmd>Trouble workspace_diagnostics<CR>]]) -- all workspace diagnostics
map("n", "<space>ac", [[<cmd>TroubleClose<CR>]])
map("n", "<space>ml", "<cmd>MetalsToggleLogs<CR>")
map("n", "<space>ma", "<cmd>lua require('telescope').extensions.metals.commands()<CR>")
map("n", "<space>gt", "<cmd>lua require('dap-go').debug_test()<CR>")
map("n", "<space>dt", "<cmd>lua require('dapui').toggle()<CR>")
map("n", "<space>dp", "<cmd>lua require('dap').toggle_breakpoint()<CR>")
map("n", "<space>dr", "<cmd>lua require('dap').run_last()<CR>")
map("n", "<space>dc", "<cmd>lua require('dap').continue()<CR>")
map("n", "<space>dh", "<cmd>lua require('dap.ui.widgets').hover()<CR>")
map("n", "<space>so", "<cmd>lua require('dap').step_over()<CR>")
map("n", "<space>si", "<cmd>lua require('dap').step_into()<CR>")
map("n", "<space>tp", "<cmd>lua require('trouble').previous({skip_groups = true, jump = true})<CR>")
map("n", "<space>tn", "<cmd>lua require('trouble').next({skip_groups = true, jump = true})<CR>")
map("n", "?", "<cmd>WhichKey<CR>")
map('i', '<c-s>', '<cmd>lua vim.lsp.buf.signature_help()<CR>')
map('n', '<c-m-h>', '<cmd>exe "vertical resize " . (winwidth(0) * 2/3)<CR>')
map('n', '<c-m-l>', '<cmd>exe "vertical resize " . (winwidth(0) * 3/2)<CR>')
map('n', '<c-m-k>', '<cmd>exe "resize " . (winheight(0) * 5/3)<CR>')
map('n', '<c-m-j>', '<cmd>exe "resize " . (winheight(0) * 3/5)<CR>')

require "lsp_signature".setup({floating_window = true})

-- completion related settings
-- This is similiar to what I use
local cmp = require("cmp")
cmp.setup({
  sources = {
    { name = "nvim_lsp" },
    { name = "vsnip" },
  },
  snippet = {
    expand = function(args)
      -- Comes from vsnip
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    -- None of this made sense to me when first looking into this since there
    -- is no vim docs, but you can't have select = true here _unless_ you are
    -- also using the snippet stuff. So keep in mind that if you remove
    -- snippets you need to remove this select
    ["<CR>"] = cmp.mapping.confirm({ select = true }),
    -- I use tabs... some say you should stick to ins-completion but this is just here as an example
    ["<Tab>"] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end,
    ["<S-Tab>"] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end,
  }),
})

----------------------------------
-- LSP Setup ---------------------
----------------------------------
local metals_config = require("metals").bare_config()

-- Example of settings
metals_config.settings = {
  showImplicitArguments = true,
--  excludedPackages = { "akka.actor.typed.javadsl", "com.github.swagger.akka.javadsl" },
}

-- *READ THIS*
-- I *highly* recommend setting statusBarProvider to true, however if you do,
-- you *have* to have a setting to display this in your statusline or else
-- you'll not see any messages from metals. There is more info in the help
-- docs about this
metals_config.init_options.statusBarProvider = "on"

-- Example if you are using cmp how to make sure the correct capabilities for snippets are set
local capabilities = vim.lsp.protocol.make_client_capabilities()
metals_config.capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

-- Debug settings if you're using nvim-dap
local dap = require("dap")

dap.configurations.scala = {
  {
    type = "scala",
    request = "launch",
    name = "RunOrTest",
    metals = {
      runType = "runOrTestFile",
      --args = { "firstArg", "secondArg", "thirdArg" }, -- here just as an example
    },
  },
  {
    type = "scala",
    request = "launch",
    name = "Test Target",
    metals = {
      runType = "testTarget",
    },
  },
}

metals_config.on_attach = function(client, bufnr)
  require("metals").setup_dap()
end

require("lspconfig").solargraph.setup{}
require("lspconfig").gdscript.setup{}

local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require("lspconfig").lua_ls.setup({
on_attach = custom_attach,
settings = {
    Lua = {
    runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = "LuaJIT",
        -- Setup your lua path
        path = runtime_path,
    },
    diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { "vim" },
    },
    workspace = {
        -- Make the server aware of Neovim runtime files
        library = api.nvim_get_runtime_file("", true),
    },
    -- Do not send telemetry data containing a randomized but unique identifier
    telemetry = {
        enable = false,
    },
    },
},
})


-- Autocmd that will actually be in charging of starting the whole thing
local nvim_metals_group = api.nvim_create_augroup("nvim-metals", { clear = true })
api.nvim_create_autocmd("FileType", {
  -- NOTE: You may or may not want java included here. You will need it if you
  -- want basic Java support but it may also conflict if you are using
  -- something like nvim-jdtls which also works on a java filetype autocmd.
  pattern = { "scala", "sbt", "java" },
  callback = function()
    require("metals").initialize_or_attach(metals_config)
  end,
  group = nvim_metals_group,
})

require'lspconfig'.starlark_rust.setup{}

require'lspconfig'.ccls.setup{}

require'lspconfig'.terraformls.setup{}
vim.api.nvim_create_autocmd({"BufWritePre"}, {
  pattern = {"*.tf", "*.tfvars"},
  callback = function()
    vim.lsp.buf.format()
  end,
})

-- You dont need to set any of these options. These are the default ones. Only
-- the loading is important
require('telescope').setup {
  defaults = {
	path_display={"smart"},
    dynamic_preview_title = true,
  },
  extensions = {
    fzf = {
      fuzzy = true,                    -- false will only do exact matching
      override_generic_sorter = true,  -- override the generic sorter
      override_file_sorter = true,     -- override the file sorter
      case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
    }
  }
}

function _G.set_terminal_keymaps()
  local opts = {buffer = 0}
  vim.keymap.set('t', '<C-t>', [[<Cmd>ToggleTerm<CR>]], opts)
  vim.keymap.set('n', '<C-t>', [[<Cmd>ToggleTerm<CR>]], opts)
end

-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd('autocmd! TermOpen term://*toggleterm#* lua set_terminal_keymaps()')

local function metals()
  if(vim.g['metals_status'] == nil) then
      return ''
  else
      return vim.g['metals_status']
  end
end

require('lualine').setup {
  options = {
    theme = 'material',
    section_separators = '', component_separators = '' 
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics', metals},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
}


-- To get fzf loaded and working with telescope, you need to call
-- load_extension, somewhere after setup function:
require('telescope').load_extension('fzf')
require("toggleterm").setup{}
require('nvim-web-devicons').setup()
require('octo').setup{
    use_local_fs = true,
}

require("which-key").setup {}
require("dapui").setup()
require("gitsigns").setup{}
require("zen-mode").setup{
    window = {
        width = 150,
        backdrop = 1,
    },
}
require("twilight").setup{
    treesitter = true,
    expand = { -- for treesitter, we we always try to expand to the top-most ancestor with these types
        "function_definition",
        "val_definition",
        "var_definition",
        "function",
        "method",
        "table",
        "if_statement",
    },
}
require('litee.lib').setup({})
require('litee.calltree').setup({})
require('aerial').setup({
  -- optionally use on_attach to set keymaps when aerial has attached to a buffer
  on_attach = function(bufnr)
    -- Jump forwards/backwards with '{' and '}'
    vim.keymap.set('n', '{', '<cmd>AerialPrev<CR>', {buffer = bufnr})
    vim.keymap.set('n', '}', '<cmd>AerialNext<CR>', {buffer = bufnr})
  end,
  layout = {
      default_direction = "prefer_left"
  }
})

require('nvim-treesitter.configs').setup {
  highlight = {
    enable = true
  },
}

local root_pattern = require('lspconfig.util').root_pattern
require("lspconfig").gopls.setup({
    root_dir = root_pattern("go.work", "go.mod", "BUILD.bazel", ".git"),
    --cmd = {"gopls", "-logfile", "/Users/loic.wisniewski/.gopls.log"},
    cmd = {"gopls-wrapper"},
    on_attach = on_attach,
    settings = {
        gopls = {
--            env = {
--                GOPACKAGESDRIVER = './tools/go/gopackagesdriver.sh'
--            },
            directoryFilters = {
                "-bazel-bin",
                "-bazel-out",
                "-bazel-testlogs",
                "-bazel-mypkg",
            },
            codelenses = {
                generate = true
            }
        },
    },

})

require('lspconfig').pylsp.setup{}

require('go').setup({
    lsp_codelens = false,
    lsp_inlay_hints = {
        enable = false,
    },
})

local format_sync_grp = vim.api.nvim_create_augroup("GoFormat", {})
vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*.go",
  callback = function()
   require('go.format').goimport()
  end,
  group = format_sync_grp,
})

require('dap-go').setup()


-- copilot
require("copilot").setup({
    suggestion = {
        auto_trigger = true,
    }
})

require("CopilotChat").setup {
    mappings = {
        reset = {
            normal = "<C-r>",
            insert = "<C-r>",
        }

    }
}

vim.g.copilot_no_tab_map = true

map("i", "<C-L>", '<cmd>lua require("copilot.suggestion").accept()<CR>')
map("i", "<C-J>", '<cmd>lua require("copilot.suggestion").next()<CR>')
map("i", "<C-K>", '<cmd>lua require("copilot.suggestion").prev()<CR>')


-- not update dignostic on insert mode
vim.lsp.handlers["textDocument/diagnostic"] = vim.lsp.with(
  vim.lsp.diagnostic.on_diagnostic, {
    -- Disable a feature
    update_in_insert = false,
  }
)

require('Comment').setup()
