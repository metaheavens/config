[settings]
throttle-ms = 50
throttle-limit = 5

[bar/top]
monitor = ${env:MONITOR:}
width = 100%
height = 27
offset-y = 5

tray-position = right
tray-padding = 2
tray-offset-y = -5

background = #115f627a
foreground = #11f2f2

overline-size = 2
overline-color = #1192f8
underline-size = 2
underline-color = #1192f8

spacing = 1
padding-right = 2
module-margin-left = 0
module-margin-right = 2

font-0 = NotoSansMNerdFontMono:size=10

modules-left = i3 bspwm
modules-center = xwindow
modules-right = battery volume cpu memory clock

[module/battery]
type = internal/battery

label-charging = Charging %percentage%%
format-charging = <animation-charging> <label-charging>
format-charging-padding = 2
format-charging-margin = 1
format-charging-font = 0
format-charging-foreground = #fff
format-charging-background = #2fbbf2
format-charging-overline = #148ebe
format-charging-underline = #148ebe

label-discharging = %percentage%%
format-discharging = <ramp-capacity> <label-discharging>
format-discharging-padding = 2
format-discharging-margin = 1
format-discharging-background = #eeeeee
format-discharging-foreground = #dd222222
format-discharging-overline = #c5c5c5
format-discharging-underline = #c5c5c5
format-discharging-font = 0
; This is useful in case the battery never reports 100% charge

label-full = Fully charged
format-full =   <label-full>
format-full-padding = 2
format-full-margin = 1
format-full-font = 0
format-full-foreground = #fff
format-full-background = #2fbbf2
format-full-overline = #148ebe
format-full-underline = #148ebe

full-at = 100
animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
; Framerate in milliseconds
animation-charging-framerate = 750

; Only applies if <animation-discharging> is used
animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-3 = 
animation-discharging-4 = 
; Framerate in milliseconds
animation-discharging-framerate = 500
; Use the following command to list batteries and adapters:
; $ ls -1 /sys/class/power_supply/
battery = BAT0
adapter = AC

; If an inotify event haven't been reported in this many
; seconds, manually poll for new values.
;
; Needed as a fallback for systems that don't report events
; on sysfs/procfs.
;
; Disable polling by setting the interval to 0.
;
; Default: 5
poll-interval = 5

[module/bspwm]
type = internal/bspwm

format = <label-state> <label-mode>

label-active = %index%
label-active-padding = 2
label-active-margin = 1
label-active-font = 0
label-active-foreground = #fff
label-active-background = #2fbbf2
label-active-overline = #148ebe
label-active-underline = #148ebe

label-occupied = %index%
label-occupied-padding = 2
label-occupied-margin = 1
label-occupied-background = #eeeeee
label-occupied-foreground = #dd222222
label-occupied-overline = #c5c5c5
label-occupied-underline = #c5c5c5
label-occupied-font = 0

label-urgent = %index%
label-urgent-padding = 2
label-urgent-margin = 1
label-urgent-font = 0

label-empty = %index%
label-empty-padding = 2
label-empty-margin = 1
label-empty-font = 0

[module/i3]
type = internal/i3

pin-workspaces = true

format = <label-state> <label-mode>
format-spacing = 0

label-focused = %index%
label-focused-padding = 2
label-focused-margin = 1
label-focused-font = 0
label-focused-foreground = #ffffff
label-focused-background = #2fbbf2
label-focused-overline = #148ebe
label-focused-underline = #148ebe

label-unfocused = %index%
label-unfocused-padding = 2
label-unfocused-margin = 1
label-unfocused-background = #eeeeee
label-unfocused-foreground = #dd222222
label-unfocused-overline = #c5c5c5
label-unfocused-underline = #c5c5c5
label-unfocused-font = 0

label-urgent = %index%
label-urgent-padding = 2
label-urgent-margin = 1
label-urgent-font = 0

label-visible = %index%
label-visible-padding = 2
label-visible-margin = 1
label-visible-font = 0

[module/cpu]
type = internal/cpu
interval = 0.5

format = <label> <ramp-coreload>
format-background = #66cc99
format-foreground = #2a5c45
format-underline = #60eaa5
format-overline = #60eaa5
format-padding = 2

label = cpu
label-font = 0

ramp-coreload-0 = ▁
ramp-coreload-0-font = 0
ramp-coreload-0-foreground = #000000
ramp-coreload-1 = ▂
ramp-coreload-1-font = 0
ramp-coreload-1-foreground = #000000
ramp-coreload-2 = ▃
ramp-coreload-2-font = 0
ramp-coreload-2-foreground = #000000
ramp-coreload-3 = ▄
ramp-coreload-3-font = 0
ramp-coreload-3-foreground = #000000
ramp-coreload-4 = ▅
ramp-coreload-4-font = 0
ramp-coreload-4-foreground = #ffffff
ramp-coreload-5 = ▆
ramp-coreload-5-font = 0
ramp-coreload-5-foreground = #ffffff
ramp-coreload-6 = ▇
ramp-coreload-6-font = 0
ramp-coreload-6-foreground = #ff3b51
ramp-coreload-7 = █
ramp-coreload-7-font = 0
ramp-coreload-7-foreground = #ff3b51

[module/memory]
type = internal/memory

format = <label> <bar-used>
format-padding = 2
format-background = #cb66cc
format-foreground = #ffe3ff
format-underline = #e58de6
format-overline = #e58de6

label = memory
label-font = 0

bar-used-width = 10
bar-used-indicator = |
bar-used-indicator-font = 0
bar-used-indicator-foreground = #ffaaf5
bar-used-fill = ─
bar-used-fill-font = 0
bar-used-fill-foreground = #ffaaf5
bar-used-empty = ─
bar-used-empty-font = 0
bar-used-empty-foreground = #934e94

[module/clock]
type = internal/date
date = %%{T3}%Y-%m-%d %H:%M%%{T-}

format-padding = 2
format-background = #ff4279
format-foreground = #ffcddc
format-underline = #ff63a5
format-overline = #ff63a5

[module/volume]
#type = internal/volume
type = internal/pulseaudio
master-mixer = Master
speaker-mixer = Speaker
headphone-mixer = Headphone
headphone-id = 9

format-volume-padding = 2
format-volume-background = #fff85a
format-volume-foreground = #43433a
format-volume-underline = #fffb8f
format-volume-overline = #fffb8f

format-muted-padding = 2
format-muted-background = #77ffffff
format-muted-foreground = #666666

label-volume = volume %percentage%
label-volume-font = 0
label-muted = sound muted
label-muted-font = 0

[module/xwindow]
type = internal/xwindow
label-font = 0
label-maxlen = 70
; vim:ft=dosini
