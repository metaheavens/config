This repository contains all my configuration files that I want to save.

To store a new file in this repository and replace it by a symbolic link,
use the script `linkify.sh` available in the root of this project.
This script should be executed at the root of this repository.

To use it, just precise the path of the file that you want to save:

`linkify.sh /User/lwisniewski/.zshrc`

Three different folder exist that will contains different files:

- `all_home` folder contains all files that can go in the home folder of any system. There will be some useless files depending on the system (for example `.i3/config` won't be useful on mac, but it will copied anyway.
- `linux` and `mac` folders contain all files that only make sense in OS directories. `linkify.sh` automatically detect if the file comes from the HOME directory or somewhere else and save it in the good directory.
