current_dir=$(pwd)

if [[ "$OSTYPE" == "linux-gnu"* ]] 
then
    all_files=$(find linux -type f)
    prefix=linux
elif [[ "$OSTYPE" == "darwin"* ]]
then
    all_mac_files=$(find mac -type f)
    prefix=mac
fi

for file in $all_files
do
file_no_prefix=${file#$prefix}
echo $file_no_prefix
if [[ -L $file_no_prefix ]]
then
    rm $file_no_prefix
fi
if [[ -f $file_no_prefix ]]
then
    echo "$file_no_prefix exists, renamed to $file_no_prefix.old"
    rm -rf $file_no_prefix.old
    mv $file_no_prefix $file_no_prefix.old
fi

rm -rf $file_no_prefix
dir_to_create=$(dirname $file_no_prefix)
mkdir -p $dir_to_create
ln -s $current_dir/$file $file_no_prefix
done
